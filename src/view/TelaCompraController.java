/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import coisas.Arrays;
import coisas.Cliente;
import coisas.Festa;
import gerenciadordefestas.Arquivo;
import gerenciadordefestas.Controller;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Brenda
 */
public class TelaCompraController extends Controller {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Label codigo;

    @FXML
    private RadioButton feminino;
    @FXML
    private RadioButton masculino;
    @FXML
    private RadioButton outro;
    @FXML
    private TextField nome;
    @FXML
    private TextField email;

    Festa party = new Festa();
  

    int x;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // CODIGO DO INGRESSO N APARECE
        codigo.setText("Código de ingresso: " + x);

    }

    public void fazerCoisa(Festa f) {
        party = f;
        party.ingressoFesta.add(f.getIngresso());
        
        //  (Festa.ingressoFesta.size()+ 1);
        x = party.getIngresso().getId();
        System.out.println(x);

    }

    @FXML
    public void venderIngresso() {
        Cliente cliente = new Cliente();
        boolean verifica = false;
        for (Cliente c : Arrays.clientes) {
            if (email.getText().equals(c.getEmail())) {
                verifica = true;
                cliente = c;
                break;
            }
        }
        if (verifica == false) {
            Arrays.clientes.add(cliente);
            System.out.println("Deu certo sepa ");
            
            if (feminino.isSelected() == true) {
                cliente.setSexo("Feminino");
            } else if (masculino.isSelected() == true) {
                cliente.setSexo("Masculino");
            } else {
                cliente.setSexo("Outro");
            }

            cliente.setNome(nome.getText());
            cliente.setEmail(email.getText());
        }

        System.out.println(cliente.getSexo());
        System.out.println(cliente.getNome());
        System.out.println(cliente.getEmail());

        cliente.addIngresso(party.getIngresso());

        Arquivo.saveCliente(Arrays.clientes);
    }

    @FXML
    public void voltar() {
        this.stage.setScene(mostrarTelaAnterior());
        this.stage.setFullScreen(false);
    }

    private Scene mostrarTelaAnterior() {
        Scene scene = null;
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(TelaController.class.getResource("Tela.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
            TelaController cc = loader.getController();
            cc.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Deu erro!");
            e.printStackTrace();
        }
        return scene;
    }

}
