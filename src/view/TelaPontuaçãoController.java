/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import coisas.Arrays;
import coisas.Cliente;
import coisas.Festa;
import gerenciadordefestas.Controller;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Brenda
 */
public class TelaPontuaçãoController extends Controller  {

        /**
         * Initializes the controller class.
         */
    
    @FXML
    private ListView<Cliente> clienteList; 
    
 
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
               
            //Adicionando dados na lista.
            for(Cliente c: Arrays.clientes){
                if (c.getPontuacao()!= 0){
                clienteList.getItems().add(c);
                }
                
            }
         this.cellFactory();           
    }    
    
    
        private void cellFactory() {
        clienteList.setCellFactory(param -> new ListCell<Cliente>() {
            @Override
            protected void updateItem(Cliente c, boolean empty) {
                if (empty) {
                 setText(null);
                } else {
                  setText("Nome: "+c.getNome()+ " Email: "+ c.getEmail()+" Pontuação: "+ c.getPontuacao());

                }
            }
        });
    }
        
@FXML
    public void voltar() {      
        this.stage.setScene(mostrarTelaAnterior());
        this.stage.setFullScreen(false);
    }
    
    private Scene mostrarTelaAnterior() {
             Scene scene = null;
        try {
            // Loader
            System.out.println("Aeeee");
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(TelaController.class.getResource("Tela.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              //observe essas duas linhas
            TelaController cc = loader.getController();
            cc.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Deu erro!");
            e.printStackTrace();
        }
        return scene;
    }        
    
    
}
